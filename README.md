# nanopb

This repository provides:
- Unmodified [nanopb] source and header files. Needed for projects that use
  nanopb generated [protobuf] code.
- Protobuf [well-known-types] generated with nanopb from the
  [unmodified .proto files][1].
- A [mongoose-os] library ready to be used in your mongoose-os app.

## mongoose-os

If you are developing a [mongoose-os] app and you want to use code that was
generated via [nanopb], just add the following to your `mos.yml`:

```yaml
libs:
  - origin: https://gitlab.com/agamigo/mgos/nanopb
```

## Well Known Types

Included [well-known-types]:
- any
- api
- descriptor
- duration
- empty
- field_mask
- source_context
- timestamp
- type

Not Included:
- struct

## Code Generation

The [`generate_c.sh`](generate_c.sh) script pulls the protobuf and nanopb code,
generates [well-known-types] C code from `.proto` files via nanopb, and copies
everything you need to `src` and `include` directories.

I will try to keep this repo up-to-date as protobuf and nanopb sources are
updated.

[nanopb]: https://github.com/nanopb/nanopb
[protobuf]: https://developers.google.com/protocol-buffers
[well-known-types]: https://developers.google.com/protocol-buffers/docs/reference/google.protobuf
[mongoose-os]: https://mongoose-os.com/docs/README.md
[1]: https://github.com/protocolbuffers/protobuf/tree/master/src/google/protobuf
