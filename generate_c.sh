#!/bin/sh
set -e

dir=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

if [ ! -d "${dir}/.build" ]; then
	mkdir "${dir}/.build"
fi

for upstream in "protocolbuffers/protobuf" "nanopb/nanopb"; do
	repo_name="$(basename ${upstream})"
	repo_dir="${dir}/.build/${repo_name}"
	if [ -d "${repo_dir}/.git" ]; then
		cd "${repo_dir}"
		git pull
	else
		clone_url="https://github.com/${upstream}.git"
		cd "${dir}/.build"
		git clone "${clone_url}"
	fi
	cd "${dir}"
done

nanopb_dir="${dir}/.build/nanopb"
nanopb_plugin="${nanopb_dir}/generator/protoc-gen-nanopb"
nanopb_plugin_arg="--plugin=protoc-gen-nanopb=${nanopb_plugin}"
protobuf_dir="${dir}/.build/protobuf"
wkt_proto_prefix_dir="${protobuf_dir}/src"
wkt_proto_dir="${wkt_proto_prefix_dir}/google/protobuf"

cd "${nanopb_dir}/generator/proto"
make
cd "${dir}"

for req_dir in "src/google/protobuf" "include/google/protobuf"
do
	if [ ! -d "${dir}/${req_dir}" ]; then
		mkdir -p "${dir}/${req_dir}"
	fi
done

# "struct.proto" fails to compile for now
for wkt in any api descriptor duration empty field_mask source_context \
	timestamp type
do
	echo "Generating nanopb C for \"${wkt}\" well known type..."
	protoc \
		"${nanopb_plugin_arg}" \
		--nanopb_out="${dir}/src" \
		--proto_path="${wkt_proto_prefix_dir}" \
		"${wkt_proto_dir}/${wkt}.proto"
	echo "Done."
done

mv ${dir}/src/google/protobuf/*.h include/google/protobuf
cp -f ${nanopb_dir}/*.h include
cp -f ${nanopb_dir}/*.c src

echo "SUCCESS!"
